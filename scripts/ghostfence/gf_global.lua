local I = require("openmw.interfaces")

local v2 = require("openmw.util").vector2
local util = require("openmw.util")
local core = require("openmw.core")
local types = require("openmw.types")
local storage = require("openmw.storage")
local world = require("openmw.world")
local async = require("openmw.async")
local acti = require("openmw.interfaces").Activation

local didCheck = false

--TODO: Add script to Azura/MSQ related object, to trigger the ghostfence disabling when that happens.

--Also, find a way to set stronghold and RR states as they happen.
local function disableFence()
    local fences = 0
    for x = -5, 10 do
        for y = 0, 14 do
            local cell = world.getExteriorCell(x, y)
            local activators = cell:getAll(types.Activator)
            local scrName = "GhostfenceScript"
            for index, obj in ipairs(activators) do
                if (obj.type.record(obj).mwscript:lower() == scrName:lower()) then
                    fences = fences + 1
                    obj.enabled = false
                end
            end
        end
    end
end
local function killActivators(cell)
    local activators = cell:getAll(types.Activator)
    local scrName = "GhostfenceScript"
    for index, obj in ipairs(activators) do
        if (obj.type.record(obj).mwscript == "" or obj.type.record(obj).mwscript == nil) then
            --if object has no script, we shouldn't disable it
        else
            local obId = obj.recordId
            if not string.find(obId, "tree") then
                obj.enabled = false --Raven rock has trees that are disabled as the building is completed, so will have to keep those.
            end
        end
    end
    for index, obj in ipairs(cell:getAll(types.Light)) do
        if (obj.type.record(obj).mwscript == "" or obj.type.record(obj).mwscript == nil) then

        else
            obj.enabled = false
        end
    end

    for index, obj in ipairs(cell:getAll(types.Door)) do
        if (obj.type.record(obj).mwscript == "" or obj.type.record(obj).mwscript == nil) then

        else
            obj.enabled = false
        end
    end

    for index, obj in ipairs(cell:getAll(types.Container)) do
        if (obj.type.record(obj).mwscript == "" or obj.type.record(obj).mwscript == nil) then

        else
            if (types.Container.capacity(obj) > 0) then
                obj.enabled = false
            end
        end
    end
end
local function isMQDone()
    local cell = world.getCellByName("Akulakhan's Chamber")

    local creatures = cell:getAll(types.Creature)
    local foundDagoth = false
    for index, crea in ipairs(creatures) do
        if (crea.recordId == "dagoth_ur_2") then
            foundDagoth = true
            local health = types.Actor.stats.dynamic.health(crea).current
            if (health < 201) then
                return true
            end
        end
    end
    if (not foundDagoth) then
        print("Didn't find dagoth ur")
        return true
    end
    return false
end
local function onInit()
    if (didCheck == false) then
        if (isMQDone()) then
            disableFence()
        end
    end
end
local function onSave()
    return { didCheck = didCheck }
end
local function onPlayerAdded(player)
    local tel = world.getExteriorCell(10, 1)
    local balIsra = world.getExteriorCell(-5, 9)
    local odai = world.getExteriorCell(-5, -5)
    local ravenRock = { world.getExteriorCell(-25, 19), world.getExteriorCell(-24, 19), world.getExteriorCell(-25, 18) }
    for index, cell in ipairs(ravenRock) do
        killActivators(cell)
    end
    killActivators(tel)
    killActivators(balIsra)
    killActivators(odai)
end
local function onLoad(data)
    if (data) then
        didCheck = data.didCheck
    end
end
return {
    interfaceName  = "Ghostfence_ZHAC",
    interface      = {
        version = 1,
        disableFence = disableFence,
        isMQDone = isMQDone,
        killActivators = killActivators,
        onPlayerAdded = onPlayerAdded,
    },
    engineHandlers = {
        onPlayerAdded = onPlayerAdded,
        onLoad = onLoad,
        onSave = onSave,
        onInit = onInit,
    },
    eventHandlers  = {
    },
}
